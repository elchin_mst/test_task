<?php

/**
 * Created by PhpStorm.
 * User: el
 * Date: 18.09.2018
 * Time: 22:12
 */
namespace CalculationBundle\Services;

use CalculationBundle\Entity\History;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CalculationService
{
    private $container;

    private $em;

    /**
     * CalculationService constructor.
     *
     * @param ContainerInterface $container
     * @param \Doctrine\ORM\EntityManager $em
     */
    function __construct(ContainerInterface $container, \Doctrine\ORM\EntityManager $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * @param $duration
     * @param $amount
     * @param $percent
     * @param $startDate
     *
     * @return array
     */
    public function calculate($duration, $amount, $percent, $startDate)
    {
        try {
            $resultArray = $this->setDate($duration, $startDate);
            $paymentArray = $this->calculatePayment($duration, $amount, $percent);
            $monthPercent = $paymentArray[0];
            $payment = $paymentArray[1];
            $newBody = $amount;

            foreach ($resultArray as $key => $value) {
                $percentHalf = bcmul($newBody, $monthPercent, 2);
                $halfOfBody = bcsub($payment, $percentHalf, 2);
                $newBody = bcsub($newBody,$halfOfBody, 2);

                $resultArray[$key]["month payment"] = $payment;
                $resultArray[$key]["month percent"] = $percentHalf;
                $resultArray[$key]["half of body"] = $halfOfBody;
                $resultArray[$key]["body"] = $newBody;
            }
            $this->saveResults($duration, $amount, $percent, $startDate, $resultArray);
            return $resultArray;
        } catch (\Exception $e) {
            $errors = array(
                $e->getMessage(),
                $e->getLine(),
                $e->getFile(),
            );
            return $errors;
        }

    }

    /**
     * @param $duration
     * @param $startDate
     *
     * @return array
     */
    public function setDate($duration, $startDate)
    {
        try {
            $date = new \DateTime($startDate);
            $interval = new \DateInterval("P1M");
            $dateArray = array(
                array(
                    "number" => 1,
                    "date" => $date->format("Y-m-d"),
                )
            );

            for($count = 2; $count <= $duration; $count++) {
                $dateArray[] = array(
                    "number" => $count,
                    "date" => $date->add($interval)->format("Y-m-d"),
                );
            }

            return $dateArray;
        } catch (\Exception $e) {
            $errors = array(
                $e->getMessage(),
                $e->getLine(),
                $e->getFile(),
            );
            return $errors;
        }
    }

    public function calculatePayment($duration, $amount, $percent)
    {
        try {
            $monthPercent = bcdiv(bcdiv($percent, 12, 10), 100, 10);
            $divisible = bcmul(
                $monthPercent,
                bcpow(
                    1 + $monthPercent,
                    $duration,
                    10
                ),
                10
            );

            $divisor = bcsub(
                bcpow(1 + $monthPercent, $duration, 10),
                    1,
                10
            );

            $coefficient = bcdiv($divisible, $divisor, 10);
            return array(
                    $monthPercent,
                    bcmul($amount, $coefficient, 2),
                );
        } catch (\Exception $e) {
            $errors = array(
                $e->getMessage(),
                $e->getLine(),
                $e->getFile(),
            );
            return $errors;
        }
    }

    public function saveResults($duration, $amount, $percent, $startDate, array $resultArray)
    {
        try {
            $inputArray = array(
                "dureation" => $duration,
                "amount" => $amount,
                "percent" => $percent,
                "start date" => $startDate,
            );
            $record = new History();
            $record->setCreateTime(new \DateTime());
            $record->setInput(json_encode($inputArray));
            $record->setOutput(json_encode($resultArray));

            $this->em->persist($record);
            $this->em->flush();

            return "successfully recorded";
        } catch (\Exception $e) {
            $errors = array(
                $e->getMessage(),
                $e->getLine(),
                $e->getFile(),
            );
            return $errors;
        }
    }
}