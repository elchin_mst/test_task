<?php

namespace CalculationBundle\Controller;

use CalculationBundle\Models\CalculatorRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

class CalculatorController extends Controller
{
    /**
     * @Route("/api/calculate")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function calculateAction(Request $request)
    {
        $calculatorRequest = new CalculatorRequest($request);
        $validator = $this->get('validator');
        $errors = $validator->validate($calculatorRequest);

        if (count($errors) > 0) {
            $errorsString = (string) $errors;

            return new Response($errorsString);
        }

        $service = $this->get("calc.service");

        return new JsonResponse(
            $service->calculate(
                $calculatorRequest->duration,
                $calculatorRequest->amount,
                $calculatorRequest->percent,
                $calculatorRequest->date
            )
        );
    }
}
