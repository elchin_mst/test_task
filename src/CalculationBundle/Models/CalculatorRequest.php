<?php

namespace CalculationBundle\Models;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CalculatorRequest
{
    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\GreaterThan(0)
     * @Assert\Expression(
     *     "this.amount / this.duration > 0.1",
     *     message="Error amount/duration < 0.01"
     * )
     *
     */
    public $duration;

    /**
     * @var float
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\GreaterThan(0)
     * @Assert\Expression(
     *     "this.amount / this.duration > 0.1",
     *     message="Error amount/duration < 0.01"
     * )
     *
     */
    public $amount;

    /**
     * @var float
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\GreaterThan(0)
     *
     */
    public $percent;

    /**
     * @var mixed
     *
     * @Assert\Date()
     * @Assert\Expression(
     *     "this.date > this.today",
     *     message="Must be more then today"
     * )
     * @Assert\NotBlank()
     * @Assert\NotNull()
     *
     */
    public $date;

    /**
     * @var mixed
     */
    public $today;

    public function __construct(Request $request)
    {
        $this->duration = $request->get("duration");
        $this->amount = $request->get("amount");
        $this->percent = $request->get("percent");
        $this->date = $request->get("date");
        $this->today = date("Y-m-d");
    }
}